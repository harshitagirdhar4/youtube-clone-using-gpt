// Add video cards to the right-pane
const videoCardsContainer = document.querySelector('.video-cards');

const dummyVideos = [
    { title: 'Video Title 1', creatorAvatar: 'images/creator-avatar1.jpg', views: '1M views', uploaded: '1 day ago' },
    { title: 'Video Title 2', creatorAvatar: 'images/creator-avatar2.jpg', views: '500K views', uploaded: '2 days ago' },
    { title: 'Video Title 3', creatorAvatar: 'images/creator-avatar3.jpg', views: '750K views', uploaded: '3 days ago' },
    // Add more dummy video data
];

function createVideoCard(video) {
    const card = document.createElement('div');
    card.classList.add('video-card');

    // Generate a random number between 1 and 10 for selecting an image
    const randomImageNumber = Math.floor(Math.random() * 10) + 1;

    card.innerHTML = `
        <img class="thumbnail" src="https://source.unsplash.com/random/250x150/?video-${randomImageNumber}" alt="Video Thumbnail">
        <div class="video-info">
            <div class="title">${video.title}</div>
            <img class="creator-avatar" src="${video.creatorAvatar}" alt="Creator Avatar">
            <div class="details">
                <div class="views">${video.views}</div>
                <div class="uploaded">${video.uploaded}</div>
            </div>
        </div>
    `;

    return card;
}

// Populate the video cards
dummyVideos.forEach(video => {
    const videoCard = createVideoCard(video);
    videoCardsContainer.appendChild(videoCard);
});
// ... Your existing JavaScript code ...

function createVideoCard(video) {
    const card = document.createElement('div');
    card.classList.add('video-card');

    const randomImageNumber = Math.floor(Math.random() * 10) + 1;

    card.innerHTML = `
        <div class="thumbnail-container">
            <img class="thumbnail" src="https://source.unsplash.com/random/250x150/?video-${randomImageNumber}" alt="Video Thumbnail">
            <div class="duration-label">2:30</div>
        </div>
        <div class="video-info">
            <img class="creator-avatar" src="${video.creatorAvatar}" alt="Creator Avatar">
            <div class="title">${video.title}</div>
        </div>
        <div class="channel-name">Channel Name</div>
        <div class="details">${video.views} • ${video.uploaded}</div>
    `;

    return card;
}

